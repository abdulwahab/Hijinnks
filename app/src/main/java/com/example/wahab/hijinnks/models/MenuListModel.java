package com.example.wahab.hijinnks.models;

/**
 * Created by Wahab on 1/10/2018.
 */

public class MenuListModel {
    private int img;
    private String name;

    public MenuListModel(int img, String name) {
        this.img = img;
        this.name = name;
    }

    public int getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public void setName(String name) {
        this.name = name;
    }
}
