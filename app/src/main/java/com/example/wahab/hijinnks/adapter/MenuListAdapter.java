package com.example.wahab.hijinnks.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wahab.hijinnks.R;
import com.example.wahab.hijinnks.models.MenuListModel;

import java.util.List;

/**
 * Created by Wahab on 1/10/2018.
 */

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.MyViewHolder> {

    private List<MenuListModel> moviesList;
    private onClickItem callBack;
    TextView menu_value_feed_msg;


    public MenuListAdapter(List<MenuListModel> moviesList) {
        this.moviesList = moviesList;
    }

    public interface onClickItem {
        public void itemClick(int position);
    }

    public void setClick(onClickItem callBack) {
        this.callBack = callBack;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.side_menu_list, parent, false);
        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        lp.height = parent.getMeasuredHeight() / 6;
        itemView.setLayoutParams(lp);

        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(MyViewHolder holder, int position) {

        MenuListModel list = moviesList.get(position);

        if (list.getName() == "Feed") {
            menu_value_feed_msg.setBackgroundResource(R.drawable.menu_black_circle);
            menu_value_feed_msg.setVisibility(View.VISIBLE);
            menu_value_feed_msg.setText("84");
            holder.name.setText(list.getName());
            holder.img.setImageResource(list.getImg());
        } else if (list.getName() == "Messages") {
            menu_value_feed_msg.setBackgroundResource(R.drawable.menu_orange_circle);
            menu_value_feed_msg.setVisibility(View.VISIBLE);
            menu_value_feed_msg.setText("3");
            holder.name.setText(list.getName());
            holder.img.setImageResource(list.getImg());
        } else {
            menu_value_feed_msg.setVisibility(View.INVISIBLE);
            holder.name.setText(list.getName());
            holder.img.setImageResource(list.getImg());
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView name;
        public ImageView img;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            img = (ImageView) view.findViewById(R.id.img);
            menu_value_feed_msg = (TextView) view.findViewById(R.id.menu_value_feed_msg);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            callBack.itemClick(getAdapterPosition());
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
