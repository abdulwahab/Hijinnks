package com.example.wahab.hijinnks.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.wahab.hijinnks.R;
import com.example.wahab.hijinnks.models.VideoDetailmodel;

import java.util.List;

/**
 * Created by Wahab on 1/11/2018.
 */

public class VideoDetailAdapter extends RecyclerView.Adapter<VideoDetailAdapter.MyViewHolder> {
    private List<VideoDetailmodel> List_view;

    public VideoDetailAdapter(List<VideoDetailmodel> list_view) {
        List_view = list_view;
    }

    @Override
    public VideoDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_detail_list, parent, false);

        return new VideoDetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VideoDetailAdapter.MyViewHolder holder, int position) {
        VideoDetailmodel frag = List_view.get(position);

        holder.video_heading.setText(frag.getHeading());
        holder.video_date.setText(frag.getVideo_date());
        holder.video_time.setText(frag.getVideo_time());
        holder.video_like.setText(frag.getLike_count());
        holder.video_rvs.setText(frag.getRvs_count());

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView video_heading;
        public TextView video_date;
        public TextView video_time;
        public TextView video_like;
        public TextView video_rvs;


        public MyViewHolder(View view) {
            super(view);

            video_heading = (TextView) view.findViewById(R.id.video_head);
            video_date = (TextView) view.findViewById(R.id.video_date);
            video_time = (TextView) view.findViewById(R.id.video_time);
            video_like = (TextView) view.findViewById(R.id.video_like);
            video_rvs = (TextView) view.findViewById(R.id.Rsvp_value);
        }
    }

    @Override
    public int getItemCount() {
        return List_view.size();
    }
}
