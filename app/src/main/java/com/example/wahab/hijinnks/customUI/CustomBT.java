package com.example.wahab.hijinnks.customUI;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Wahab on 1/31/2018.
 */

@SuppressLint("AppCompatCustomView")
public class CustomBT extends Button {
    public CustomBT(Context context) {
        super(context);
    }

    public CustomBT(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomBT(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
