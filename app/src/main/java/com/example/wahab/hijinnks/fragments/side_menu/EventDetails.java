package com.example.wahab.hijinnks.fragments.side_menu;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.wahab.hijinnks.R;


public class EventDetails extends Fragment {

    TextView description, locationEvent, dateTime, intersts, publicText, privateText;
    View DescriptionEvent_active, PinEvent_active, DateTimeEvent_active, interestEvent_active;
    ScrollView scrollview_event;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitView(view);
        InitListener();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event_details, container, false);

        return view;
    }

    private void InitView(View view) {
        DescriptionEvent_active = (View) view.findViewById(R.id.DescriptionEvent_active);
        PinEvent_active = (View) view.findViewById(R.id.PinEvent_active);
        DateTimeEvent_active = (View) view.findViewById(R.id.DateTimeEvent_active);
        interestEvent_active = (View) view.findViewById(R.id.interestEvent_active);


        description = (TextView) view.findViewById(R.id.description);
        locationEvent = (TextView) view.findViewById(R.id.locationEvent);
        dateTime = (TextView) view.findViewById(R.id.dateTime);
        intersts = (TextView) view.findViewById(R.id.intersts);

        scrollview_event = (ScrollView) view.findViewById(R.id.scrollview_event);
    }

    private void InitListener() {
        scrollview_event.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                Rect scrollBounds = new Rect();
                scrollview_event.getHitRect(scrollBounds);

                if (description.getLocalVisibleRect(scrollBounds)) {
                    // if layout even a single pixel, is within the visible area do something
//                    scrollview_event.scrollTo(0, description.getTop());

                    DescriptionEvent_active.setVisibility(View.VISIBLE);
                    PinEvent_active.setVisibility(View.INVISIBLE);
                    DateTimeEvent_active.setVisibility(View.INVISIBLE);
                    interestEvent_active.setVisibility(View.INVISIBLE);
                } else if (locationEvent.getLocalVisibleRect(scrollBounds)) {
                    DescriptionEvent_active.setVisibility(View.INVISIBLE);
                    PinEvent_active.setVisibility(View.VISIBLE);
                    DateTimeEvent_active.setVisibility(View.INVISIBLE);
                    interestEvent_active.setVisibility(View.INVISIBLE);
                } else if (dateTime.getLocalVisibleRect(scrollBounds)) {
                    DescriptionEvent_active.setVisibility(View.INVISIBLE);
                    PinEvent_active.setVisibility(View.INVISIBLE);
                    DateTimeEvent_active.setVisibility(View.VISIBLE);
                    interestEvent_active.setVisibility(View.INVISIBLE);
                } else if (intersts.getLocalVisibleRect(scrollBounds)) {


                    DescriptionEvent_active.setVisibility(View.INVISIBLE);
                    PinEvent_active.setVisibility(View.INVISIBLE);
                    DateTimeEvent_active.setVisibility(View.INVISIBLE);
                    interestEvent_active.setVisibility(View.VISIBLE);
                }
            }
        });
    }

}
