package com.example.wahab.hijinnks.models;

import java.util.List;

/**
 * Created by Wahab on 1/3/2018.
 */

public class HomeListModel {

    //top bar
    private int profile_img;
    private String user_name;
    private String user_address;
    private  String distance;

    //center
    private int video;

    private String item_name;
    private String date;
    private String time;

    private String description;

    //bottom
    private String comment;
    private String views;
    private String like;

    public HomeListModel(int profile_img, String user_name, String user_address, String distance, int video, String item_name, String date, String time, String description, String comment, String views, String like) {
        this.profile_img = profile_img;
        this.user_name = user_name;
        this.user_address = user_address;
        this.distance = distance;
        this.video = video;
        this.item_name = item_name;
        this.date = date;
        this.time = time;
        this.description = description;
        this.comment = comment;
        this.views = views;
        this.like = like;
    }

    public HomeListModel(List<HomeListModel> list) {

    }

    public int getProfile_img() {
        return profile_img;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getUser_address() {
        return user_address;
    }

    public String getDistance() {
        return distance;
    }

    public int getVideo() {
        return video;
    }

    public String getItem_name() {
        return item_name;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getDescription() {
        return description;
    }

    public String getComment() {
        return comment;
    }

    public String getViews() {
        return views;
    }

    public String getLike() {
        return like;
    }

    public void setProfile_img(int profile_img) {
        this.profile_img = profile_img;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setUser_address(String user_address) {
        this.user_address = user_address;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public void setVideo(int video) {
        this.video = video;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public void setLike(String like) {
        this.like = like;
    }
}
