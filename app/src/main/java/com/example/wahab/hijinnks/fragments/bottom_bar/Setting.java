package com.example.wahab.hijinnks.fragments.bottom_bar;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.wahab.hijinnks.R;

import de.hdodenhof.circleimageview.CircleImageView;


public class Setting extends Fragment {
    RelativeLayout main_layout;
    LinearLayout form_layout, advanced_text, imageLayout, bottomLayout;
    RelativeLayout more_option, banner_img;
    CircleImageView profile_image;
    ImageView camera_img;
    View view_line;
    float d_value, d_value1;

    public Setting() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitView(view);
        InitListener();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        return view;
    }

    private void InitView(View view) {
        main_layout = view.findViewById(R.id.main_layout);
        view_line = view.findViewById(R.id.view_line);

        banner_img = view.findViewById(R.id.banner_img);
        profile_image = view.findViewById(R.id.profile_image);
        camera_img = view.findViewById(R.id.camera_img);
        form_layout = view.findViewById(R.id.form_layout);
        advanced_text = view.findViewById(R.id.advanced_text);
        more_option = view.findViewById(R.id.more_option);

        imageLayout = view.findViewById(R.id.imageLayout);
        bottomLayout = view.findViewById(R.id.bottomLayout);
    }

    private void InitListener() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                d_value = main_layout.getMeasuredHeight();
                if (d_value <= 970 ) {
                    d_value = d_value / 2.3F;
                    LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) banner_img.getLayoutParams();
                    param.height = 200;
                    banner_img.setLayoutParams(param);
//                    banner_img.setMaxHeight(100);
                } else {
                    d_value = d_value / 2.5F;
                }

                float mid = d_value;
                float upper = d_value;
                float bottom = d_value;

//                divide_function(imageLayout, upper);
                divide_function(form_layout, mid);
                divide_function(bottomLayout, bottom);

            }
        }, 100);

        more_option.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {

                android.support.v4.app.FragmentTransaction fragmentTransaction;
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.frame_layout, new DetailsSetting());
                fragmentTransaction.commitAllowingStateLoss();
            }
        });
    }


    private void divide_function(View v, float value) {

        if (v instanceof LinearLayout) {
            RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) v.getLayoutParams();
            param.height = (int) value;
            v.setLayoutParams(param);
        } else if (v instanceof RelativeLayout) {
            RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) v.getLayoutParams();
            param.height = (int) value;
            v.setLayoutParams(param);
        }
    }

}
