package com.example.wahab.hijinnks.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wahab.hijinnks.R;

public class Login extends AppCompatActivity {
    TextView btn_login, btn_signup;
    View view1, view2;
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        android.support.v4.app.FragmentTransaction fragmentTransaction;
//        fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.frame_login, new LoginScreen());
//        fragmentTransaction.commitAllowingStateLoss();
//
//        btn_login = (TextView) findViewById(R.id.btn_login);
//        view1 = (View) findViewById(R.id.login_view);
//        view2 = (View) findViewById(R.id.signup_view);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        InitView();
        InitListener();
//        btn_login.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("ResourceAsColor")
//            @Override
//            public void onClick(View v) {
//
//
//                view1.setVisibility(View.VISIBLE);
//
//
//                view2.setVisibility(View.INVISIBLE);
//
//                btn_login.setTextColor(Color.parseColor("#ffffff"));
//                btn_signup.setTextColor(Color.parseColor("#9d948f"));
//
//                android.support.v4.app.FragmentTransaction fragmentTransaction;
//                fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.frame_login, new LoginScreen());
//                fragmentTransaction.commitAllowingStateLoss();
//            }
//        });
//
//        btn_signup = (TextView) findViewById(R.id.btn_signup);
//        btn_signup.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("ResourceAsColor")
//            @Override
//            public void onClick(View v) {
//
//
//                view1.setVisibility(View.INVISIBLE);
//
//
//                view2.setVisibility(View.VISIBLE);
//
//                btn_signup.setTextColor(Color.parseColor("#ffffff"));
//                btn_login.setTextColor(Color.parseColor("#9d948f"));
//
//                android.support.v4.app.FragmentTransaction fragmentTransaction;
//                fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.frame_login, new SignupScreen());
//                fragmentTransaction.commitAllowingStateLoss();
//            }
//        });
//    }
    }

    private void InitListener() {
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent variable = new Intent(Login.this, BottomTabBar.class);
                startActivity(variable);
            }
        });
    }

    private void InitView() {
        img = (ImageView) findViewById(R.id.login_button);
    }
}
