package com.example.wahab.hijinnks.models;

/**
 * Created by Wahab on 1/4/2018.
 */

public class SearchListModel {

    private int user_profile;
    private String name_user;
    private String invites_count;
    private int follow_type;

    public SearchListModel(int user_profile, String name_user, String invites_count, int follow_type) {
        this.user_profile = user_profile;
        this.name_user = name_user;
        this.invites_count = invites_count;
        this.follow_type = follow_type;
    }

    public int getUser_profile() {
        return user_profile;
    }

    public String getName_user() {
        return name_user;
    }

    public String getInvites_count() {
        return invites_count;
    }

    public int getFollow_type() {
        return follow_type;
    }

    public void setUser_profile(int user_profile) {
        this.user_profile = user_profile;
    }

    public void setName_user(String name_user) {
        this.name_user = name_user;
    }

    public void setInvites_count(String invites_count) {
        this.invites_count = invites_count;
    }

    public void setFollow_type(int follow_type) {
        this.follow_type = follow_type;
    }
}
