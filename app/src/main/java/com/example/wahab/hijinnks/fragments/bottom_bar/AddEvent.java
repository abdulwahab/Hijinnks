package com.example.wahab.hijinnks.fragments.bottom_bar;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.wahab.hijinnks.R;


public class AddEvent extends Fragment {
    TextView description, locationEvent, dateTime, intersts, set_hour, set_min, set_pm;
    View DescriptionEvent_active, PinEvent_active, DateTimeEvent_active, interestEvent_active;
    ScrollView scrollview_event;
    Button cover, slider;
    Spinner dropdown, mint, ampm;
    ImageView public_icon, private_icon;
    RelativeLayout hour, min, pm;
    ArrayAdapter<String> adapter, adapter_mint, adapter_ampm;
    boolean public1 = true, private1 = true;
    String[] items = new String[]{"Hours", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
    String[] items_mint = new String[]{"Min", "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60"};
    String[] items_ampm = new String[]{"PM", "AM"};

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitView(view);
        InitListener();
        InitSapiner();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_event, container, false);

        return view;
    }

    private void InitView(View view) {
        public_icon = view.findViewById(R.id.public_icon);
        private_icon = view.findViewById(R.id.private_icon);

        cover = (Button) view.findViewById(R.id.coverPhoto);
        slider = (Button) view.findViewById(R.id.slider);

        hour = view.findViewById(R.id.hour);
        min = view.findViewById(R.id.min);
        pm = view.findViewById(R.id.pm);

        set_hour = view.findViewById(R.id.set_hour);
        set_min = view.findViewById(R.id.set_min);
        set_pm = view.findViewById(R.id.set_pm);

        dropdown = (Spinner) view.findViewById(R.id.hours);
        mint = (Spinner) view.findViewById(R.id.mint);
        ampm = (Spinner) view.findViewById(R.id.am_pm);

        DescriptionEvent_active = (View) view.findViewById(R.id.DescriptionEvent_active);
        PinEvent_active = (View) view.findViewById(R.id.PinEvent_active);
        DateTimeEvent_active = (View) view.findViewById(R.id.DateTimeEvent_active);
        interestEvent_active = (View) view.findViewById(R.id.interestEvent_active);


        description = (TextView) view.findViewById(R.id.description);
        locationEvent = (TextView) view.findViewById(R.id.locationEvent);
        dateTime = (TextView) view.findViewById(R.id.dateTime);
        intersts = (TextView) view.findViewById(R.id.intersts);

        scrollview_event = (ScrollView) view.findViewById(R.id.scrollview_event);

        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        adapter_mint = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items_mint);
        mint.setAdapter(adapter_mint);

        adapter_ampm = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items_ampm);
        ampm.setAdapter(adapter_ampm);
    }

    private void InitSapiner() {
        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                set_hour.setText(adapter.getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mint.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                set_min.setText(adapter_mint.getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ampm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                set_pm.setText(adapter_ampm.getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void InitListener() {
        public_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (public1 == true) {

                    public_icon.setImageResource(R.drawable.public_check1);
                    private_icon.setImageResource(R.drawable.private_check1);

                    public1 = false;
                } else {

                    public_icon.setImageResource(R.drawable.public_check);
                    private_icon.setImageResource(R.drawable.private_check);

                    public1 = true;
                }
            }
        });

        private_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (private1 == true) {

                    private_icon.setImageResource(R.drawable.private_check1);
                    public_icon.setImageResource(R.drawable.public_check1);

                    private1 = false;
                } else {

                    private_icon.setImageResource(R.drawable.private_check);
                    public_icon.setImageResource(R.drawable.public_check);

                    private1 = true;
                }
            }
        });
        cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cover.setBackgroundResource(R.drawable.btn_border_blue);
                slider.setBackgroundResource(0);

            }
        });
        slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slider.setBackgroundResource(R.drawable.btn_border_blue);
                cover.setBackgroundResource(0);

            }
        });

        scrollview_event.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                Rect scrollBounds = new Rect();
                scrollview_event.getHitRect(scrollBounds);

                if (description.getLocalVisibleRect(scrollBounds)) {
                    // if layout even a single pixel, is within the visible area do something
//                    scrollview_event.scrollTo(0, description.getTop());

                    DescriptionEvent_active.setVisibility(View.VISIBLE);
                    PinEvent_active.setVisibility(View.INVISIBLE);
                    DateTimeEvent_active.setVisibility(View.INVISIBLE);
                    interestEvent_active.setVisibility(View.INVISIBLE);
                } else if (locationEvent.getLocalVisibleRect(scrollBounds)) {
                    DescriptionEvent_active.setVisibility(View.INVISIBLE);
                    PinEvent_active.setVisibility(View.VISIBLE);
                    DateTimeEvent_active.setVisibility(View.INVISIBLE);
                    interestEvent_active.setVisibility(View.INVISIBLE);
                } else if (dateTime.getLocalVisibleRect(scrollBounds)) {
                    DescriptionEvent_active.setVisibility(View.INVISIBLE);
                    PinEvent_active.setVisibility(View.INVISIBLE);
                    DateTimeEvent_active.setVisibility(View.VISIBLE);
                    interestEvent_active.setVisibility(View.INVISIBLE);
                } else if (intersts.getLocalVisibleRect(scrollBounds)) {


                    DescriptionEvent_active.setVisibility(View.INVISIBLE);
                    PinEvent_active.setVisibility(View.INVISIBLE);
                    DateTimeEvent_active.setVisibility(View.INVISIBLE);
                    interestEvent_active.setVisibility(View.VISIBLE);
                }
            }
        });

        hour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dropdown.performClick();
            }
        });
        min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mint.performClick();
            }
        });
        pm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ampm.performClick();
            }
        });
    }

}
