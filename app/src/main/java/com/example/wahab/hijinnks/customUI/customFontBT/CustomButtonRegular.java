package com.example.wahab.hijinnks.customUI.customFontBT;

import android.content.Context;
import android.graphics.Typeface;
import android.text.util.Linkify;
import android.util.AttributeSet;

import com.example.wahab.hijinnks.customUI.CustomBT;
import com.example.wahab.hijinnks.customUI.CustomET;
import com.example.wahab.hijinnks.customUI.FontCache;


/**
 * Created by Wahab on 1/31/2018.
 */

public class CustomButtonRegular extends CustomBT {
    public CustomButtonRegular(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public CustomButtonRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public CustomButtonRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("VarelaRound-Regular.ttf", context);
        setTypeface(customFont, Typeface.NORMAL);
        setLinksClickable(true);
        setAutoLinkMask(Linkify.WEB_URLS);
    }

}
