package com.example.wahab.hijinnks.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.wahab.hijinnks.R;
import com.example.wahab.hijinnks.models.HomeListModel;

import java.util.List;

/**
 * Created by Wahab on 1/3/2018.
 */

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.MyViewHolder> {
    private List<HomeListModel> List_view;


    public HomeListAdapter(List<HomeListModel> list_view) {
        List_view = list_view;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_list, parent, false);

        return new HomeListAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        HomeListModel frag = List_view.get(position);

        holder.profile_img.setImageResource(frag.getProfile_img());
        holder.user_name.setText(frag.getUser_name());
        holder.user_address.setText(frag.getUser_address());
        holder.distance.setText(frag.getDistance());

        holder.video.setBackgroundResource(frag.getVideo());


        holder.item_name.setText(frag.getItem_name());
        holder.date.setText(frag.getDate());
        holder.time.setText(frag.getTime());

        holder.description.setText(frag.getDescription());
        holder.comment.setText(frag.getComment());
        holder.views.setText(frag.getViews());
        holder.like.setText(frag.getLike());

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //top
        public ImageView profile_img;
        public TextView user_name;
        public TextView user_address;
        public TextView distance;

        //center
        public RelativeLayout video;

        public TextView item_name;
        public TextView date;
        public TextView time;

        public TextView description;

        //bottom
        public TextView comment;
        public TextView views;
        public TextView like;

        public MyViewHolder(View view) {
            super(view);

            profile_img = (ImageView) view.findViewById(R.id.pro_img);

            user_name = (TextView) view.findViewById(R.id.userName);
            user_address = (TextView) view.findViewById(R.id.userAddress);
            distance = (TextView) view.findViewById(R.id.userDistance);

            video = (RelativeLayout) view.findViewById(R.id.video_bg);

            item_name = (TextView) view.findViewById(R.id.itemName);
            date = (TextView) view.findViewById(R.id.post_date);
            time = (TextView) view.findViewById(R.id.post_time);

            description = (TextView) view.findViewById(R.id.post_description);

            comment = (TextView) view.findViewById(R.id.comment_count);
            views = (TextView) view.findViewById(R.id.views_count);
            like = (TextView) view.findViewById(R.id.like_count);


        }
    }

    @Override
    public int getItemCount() {
        return List_view.size();
    }
}
