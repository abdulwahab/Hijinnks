package com.example.wahab.hijinnks.network.model;

import com.example.wahab.hijinnks.data_structure.APIActions;

public interface APIResponseListner {
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions);
    public void onRequestError(String response, APIActions.ApiActions apiActions);
}
