package com.example.wahab.hijinnks.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wahab.hijinnks.R;
import com.example.wahab.hijinnks.models.SearchListModel;

import java.util.List;

/**
 * Created by Wahab on 1/4/2018.
 */

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.MyViewHolder> {


    private List<SearchListModel> List_view;


    public SearchListAdapter(List<SearchListModel> list_view) {
        List_view = list_view;
    }

    @Override
    public SearchListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_list, parent, false);

        return new SearchListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchListAdapter.MyViewHolder holder, int position) {
        SearchListModel frag = List_view.get(position);

        holder.user_profile.setImageResource(frag.getUser_profile());
        holder.name_user.setText(frag.getName_user());
        holder.invites_count.setText(frag.getInvites_count());
        holder.follow_type.setImageResource(frag.getFollow_type());

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //top
        public ImageView user_profile;
        public TextView name_user;
        public TextView invites_count;
        public ImageView follow_type;


        public MyViewHolder(View view) {
            super(view);

            user_profile = (ImageView) view.findViewById(R.id.profile_img_searc);
            name_user = (TextView) view.findViewById(R.id.search_name);
            invites_count = (TextView) view.findViewById(R.id.invite_count);
            follow_type = (ImageView) view.findViewById(R.id.btn_follow);


        }
    }

    @Override
    public int getItemCount() {
        return List_view.size();
    }
}


