package com.example.wahab.hijinnks.fragments.side_menu;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.wahab.hijinnks.R;
import com.example.wahab.hijinnks.adapter.VideoDetailAdapter;
import com.example.wahab.hijinnks.fragments.bottom_bar.DetailsSetting;
import com.example.wahab.hijinnks.models.VideoDetailmodel;

import java.util.ArrayList;
import java.util.List;

public class Profile extends Fragment {
    private List<VideoDetailmodel> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private VideoDetailAdapter homeAdapter;

    LinearLayout invites, rvs;
    TextView invitValue, inviteText, rvs_title, rvsValue;
    View invite_view, rvs_view;
    ImageView back_icon;

    public Profile() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitView(view);
        InitRecyclerView();
        InitListener();
    }

    private void InitView(View view) {
        back_icon = view.findViewById(R.id.back_icon);

        invites = view.findViewById(R.id.invites);
        rvs = view.findViewById(R.id.rvs);

        invitValue = view.findViewById(R.id.invitValue);
        inviteText = view.findViewById(R.id.invite_text);
        invite_view = view.findViewById(R.id.invite_view);

        rvs_title = view.findViewById(R.id.rvs_title);
        rvsValue = view.findViewById(R.id.rvsValue);
        rvs_view = view.findViewById(R.id.rvs_view);

        recyclerView = view.findViewById(R.id.recycler_view_video_detail);

        homeAdapter = new VideoDetailAdapter(list);
    }

    private void InitListener() {
        prepareFragmentData();

        invites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inviteText.setTextColor(Color.parseColor("#1f4ba5"));
                invitValue.setTextColor(Color.parseColor("#1f4ba5"));
                invite_view.setVisibility(View.VISIBLE);

                rvs_title.setTextColor(Color.parseColor("#e8e8e8"));
                rvsValue.setTextColor(Color.parseColor("#e8e8e8"));
                rvs_view.setVisibility(View.INVISIBLE);

                prepareFragmentData();
            }
        });

        rvs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inviteText.setTextColor(Color.parseColor("#e8e8e8"));
                invitValue.setTextColor(Color.parseColor("#e8e8e8"));
                invite_view.setVisibility(View.INVISIBLE);

                rvs_title.setTextColor(Color.parseColor("#1f4ba5"));
                rvsValue.setTextColor(Color.parseColor("#1f4ba5"));
                rvs_view.setVisibility(View.VISIBLE);

                prepareFragmentData();
            }
        });

        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentTransaction fragmentTransaction;
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.remove(Profile.this);
                fragmentTransaction.commitAllowingStateLoss();
            }
        });
    }

    private void InitRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(homeAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        return view;
    }

    private void prepareFragmentData() {
        list.clear();

        VideoDetailmodel list_data = new VideoDetailmodel("Event & Performance", "Augest 10,2017", "6:00 PM", "2", "100");
        list.add(list_data);

        list_data = new VideoDetailmodel("Welcome to  Ziro Valley 2017", "Augest 12,2017", "6:00 PM", "20", "100");
        list.add(list_data);

        list_data = new VideoDetailmodel("Zachary Stevenson Performs", "Augest 15,2017", "6:00 PM", "2k", "100");
        list.add(list_data);

        list_data = new VideoDetailmodel("How to watch Microsoft's Office", "Augest 19,2017", "6:00 PM", "0", "100");
        list.add(list_data);

        list_data = new VideoDetailmodel("Welcome to  Ziro Valley 2017", "Augest 12,2017", "6:00 PM", "20", "100");
        list.add(list_data);

        list_data = new VideoDetailmodel("Zachary Stevenson Performs", "Augest 15,2017", "6:00 PM", "2k", "100");
        list.add(list_data);

        list_data = new VideoDetailmodel("How to watch Microsoft's Office", "Augest 19,2017", "6:00 PM", "0", "100");
        list.add(list_data);


        homeAdapter.notifyDataSetChanged();
    }

}
