package com.example.wahab.hijinnks.fragments.bottom_bar;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.wahab.hijinnks.R;
import com.example.wahab.hijinnks.adapter.SearchListAdapter;
import com.example.wahab.hijinnks.models.SearchListModel;

import java.util.ArrayList;
import java.util.List;


public class SearchFriend extends Fragment {
    private List<SearchListModel> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private SearchListAdapter searchAdapter;

    TextView all, following, follower;
    View viewAll, viewFollowing, viewFollower;

    public SearchFriend() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitView(view);
        InitListener();
        InitRecyclerView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        return view;
    }


    private void InitView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_seachAll_fragment);
        searchAdapter = new SearchListAdapter(list);

        all = (TextView) view.findViewById(R.id.tab_all);
        follower = (TextView) view.findViewById(R.id.tab_follower);
        following = (TextView) view.findViewById(R.id.tab_following);

        viewAll = (View) view.findViewById(R.id.tab_all_bar);
        viewFollowing = (View) view.findViewById(R.id.tab_following_bar);
        viewFollower = (View) view.findViewById(R.id.tab_follower_bar);
    }

    private void InitListener() {
        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewAll.setVisibility(View.VISIBLE);
                viewFollowing.setVisibility(View.INVISIBLE);
                viewFollower.setVisibility(View.INVISIBLE);

                all.setTextColor(Color.parseColor("#1f4ba5"));
                follower.setTextColor(Color.parseColor("#e8e8e8"));
                following.setTextColor(Color.parseColor("#e8e8e8"));

                prepareFragmentData();
            }
        });

        follower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewAll.setVisibility(View.INVISIBLE);
                viewFollowing.setVisibility(View.INVISIBLE);
                viewFollower.setVisibility(View.VISIBLE);

                all.setTextColor(Color.parseColor("#e8e8e8"));
                follower.setTextColor(Color.parseColor("#1f4ba5"));
                following.setTextColor(Color.parseColor("#e8e8e8"));

                prepareFragmentData_Followers();
            }
        });

        following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewAll.setVisibility(View.INVISIBLE);
                viewFollowing.setVisibility(View.VISIBLE);
                viewFollower.setVisibility(View.INVISIBLE);

                all.setTextColor(Color.parseColor("#e8e8e8"));
                follower.setTextColor(Color.parseColor("#e8e8e8"));
                following.setTextColor(Color.parseColor("#1f4ba5"));

                prepareFragmentData_Following();
            }
        });
    }

    private void InitRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(searchAdapter);
        prepareFragmentData();
    }

    private void prepareFragmentData() {
        list.clear();
        SearchListModel list_data = new SearchListModel(R.drawable.profile1, "Mona Liza", "32", R.drawable.following_orange);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile2, "Abubakar Butt", "32", R.drawable.followed_blue);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile3, "White cam", "232", R.drawable.follow_gray);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile1, "Mona Liza", "32", R.drawable.following_orange);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile2, "Abubakar Butt", "32", R.drawable.followed_blue);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile3, "White cam", "232", R.drawable.follow_gray);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile2, "Abubakar Butt", "32", R.drawable.followed_blue);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile3, "White cam", "232", R.drawable.follow_gray);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile2, "Abubakar Butt", "32", R.drawable.followed_blue);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile3, "White cam", "232", R.drawable.follow_gray);
        list.add(list_data);


        searchAdapter.notifyDataSetChanged();
    }

    private void prepareFragmentData_Followers() {
        list.clear();
        SearchListModel list_data = new SearchListModel(R.drawable.profile1, "Mona Liza", "32", R.drawable.followed_blue);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile2, "Abubakar Butt", "32", R.drawable.followed_blue);
        list.add(list_data);


        list_data = new SearchListModel(R.drawable.profile3, "Mona Liza", "132", R.drawable.followed_blue);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile1, "Abubakar Butt", "32", R.drawable.followed_blue);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile2, "Abubakar Butt", "32", R.drawable.followed_blue);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile3, "Mona Liza", "132", R.drawable.followed_blue);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile1, "Abubakar Butt", "32", R.drawable.followed_blue);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile2, "Mona Liza", "132", R.drawable.followed_blue);
        list.add(list_data);


        list_data = new SearchListModel(R.drawable.profile3, "Mona Liza", "132", R.drawable.followed_blue);
        list.add(list_data);


        searchAdapter.notifyDataSetChanged();
    }

    private void prepareFragmentData_Following() {
        list.clear();
        SearchListModel list_data = new SearchListModel(R.drawable.profile1, "Mona Liza", "32", R.drawable.follow_gray);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile2, "Abubakar Butt", "32", R.drawable.follow_gray);
        list.add(list_data);


        list_data = new SearchListModel(R.drawable.profile3, "Mona Liza", "132", R.drawable.follow_gray);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile1, "Abubakar Butt", "32", R.drawable.follow_gray);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile2, "Abubakar Butt", "32", R.drawable.follow_gray);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile3, "Mona Liza", "132", R.drawable.follow_gray);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile1, "Abubakar Butt", "32", R.drawable.follow_gray);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile2, "Mona Liza", "132", R.drawable.follow_gray);
        list.add(list_data);

        list_data = new SearchListModel(R.drawable.profile3, "Mona Liza", "132", R.drawable.follow_gray);
        list.add(list_data);


        searchAdapter.notifyDataSetChanged();
    }

}
