package com.example.wahab.hijinnks.fragments.authentication;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.wahab.hijinnks.R;
import com.example.wahab.hijinnks.activities.BottomTabBar;


public class LoginScreen extends Fragment {
     ImageView img;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_login_screen, container, false);

        img = (ImageView) view.findViewById(R.id.login_button);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent variable = new Intent(getActivity() , BottomTabBar.class);
                startActivity(variable);
            }
        });

        return view;
    }


}
