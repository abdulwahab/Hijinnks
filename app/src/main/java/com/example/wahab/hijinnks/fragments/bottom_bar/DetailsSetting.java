package com.example.wahab.hijinnks.fragments.bottom_bar;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.example.wahab.hijinnks.R;


public class DetailsSetting extends Fragment {

    ImageView back_arrow;
    LinearLayout form_layout,setting_main_l, accountl, settingl, connectl;
    RelativeLayout changeR, privateR, linkedR, pushR, interestR, fbR, twitterR;

    float divide_value, divide_value1;
    LinearLayout scroll;
    private ProgressBar progress;


    public DetailsSetting() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitView(view);
        InitListener();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details_setting, container, false);

//        LinearLayout tv = new LinearLayout(view.getContext());
//        tv.setLayoutParams(new TableLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));

        return view;
    }

    private void InitListener() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                divide_value = setting_main_l.getMeasuredHeight();
                divide_value1 = divide_value / 11;
                float xyz = divide_value1;

                divide_function(accountl, xyz);
                divide_function(changeR, xyz);
                divide_function(privateR, xyz);
                divide_function(settingl, xyz);
                divide_function(linkedR, xyz);
                divide_function(pushR, xyz);
                divide_function(interestR, xyz);
                divide_function(connectl, xyz);
                divide_function(fbR, xyz);
                divide_function(twitterR, xyz);

                progress.setVisibility(View.GONE);
                scroll.setVisibility(View.VISIBLE);
//                scroll.setSmoothScrollingEnabled(true);

            }
        }, 100);

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentTransaction fragmentTransaction;
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.remove(DetailsSetting.this);
                fragmentTransaction.commitAllowingStateLoss();

            }

        });
    }

    private void InitView(View view) {
        form_layout = view.findViewById(R.id.form_layout);

        setting_main_l = view.findViewById(R.id.setting_main_l);
        scroll = view.findViewById(R.id.scroll);
        progress = view.findViewById(R.id.progress);

        accountl = view.findViewById(R.id.accountl);
        changeR = view.findViewById(R.id.changeR);
        privateR = view.findViewById(R.id.privateR);
        settingl = view.findViewById(R.id.settingl);
        linkedR = view.findViewById(R.id.linkedR);
        pushR = view.findViewById(R.id.pushR);
        interestR = view.findViewById(R.id.interestR);
        connectl = view.findViewById(R.id.connectl);
        fbR = view.findViewById(R.id.fbR);
        twitterR = view.findViewById(R.id.twitterR);

        back_arrow = view.findViewById(R.id.back_arrow);
    }

    private void divide_function(View v, float value) {

        if (v instanceof LinearLayout) {
            LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) v.getLayoutParams();
            param.height = (int) value;
            v.setLayoutParams(param);
        } else if (v instanceof RelativeLayout) {
            LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) v.getLayoutParams();
            param.height = (int) value;
            v.setLayoutParams(param);
        }
    }

}
