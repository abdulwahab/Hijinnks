package com.example.wahab.hijinnks.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.wahab.hijinnks.R;
import com.example.wahab.hijinnks.adapter.CommentListAdapter;
import com.example.wahab.hijinnks.models.CommentListModal;

import java.util.ArrayList;
import java.util.List;

import static com.example.wahab.hijinnks.activities.BottomTabBar.ChangeStatusBarColor;

public class CommentScreen extends AppCompatActivity {
    private List<CommentListModal> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private CommentListAdapter commentAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ChangeStatusBarColor(CommentScreen.this, "#00000000");
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//            getWindow().setStatusBarColor(Color.TRANSPARENT);
//        }
        setContentView(R.layout.activity_comment_screen);

        InitView();
        InitListener();
        InitRecyclerView();

    }

    private void InitView() {
        recyclerView = findViewById(R.id.recycler_view_comment);
        commentAdapter = new CommentListAdapter(list);
    }

    private void InitListener() {
        prepareFragmentData();
    }

    private void InitRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(commentAdapter);
    }

    private void prepareFragmentData() {
        CommentListModal list_data = new CommentListModal(R.drawable.profile5, "Abdul wahab", "2min ago", "A belief that there are better days ahead. These needs will be met only if we act boldly in the years ahead; and if we understand that the challenges we face are shared, and our failure to meet them will hurt us AllSearch.");
        list.add(list_data);

        list_data = new CommentListModal(R.drawable.profile4, "Earl Atkins", "2min ago", "A belief that there are better days ahead. These needs will be met only if we act boldly in the years ahead; and if we understand that the challenges we face are shared, and our failure to meet them will hurt us AllSearch.");
        list.add(list_data);

        list_data = new CommentListModal(R.drawable.profile5, "AbuBakar Butt", "1day ago", "And that is why we are providing more than $2.8 billion to help Afghans develop their economy and deliver services that people depend upon.");
        list.add(list_data);

        list_data = new CommentListModal(R.drawable.profile4, "Leonard Perez", "1day ago", "And that is why we are providing more than $2.8 billion to help Afghans develop their economy and deliver services that people depend upon.");
        list.add(list_data);

        list_data = new CommentListModal(R.drawable.profile_icon, "AbuBakar Butt", "1day ago", "And that is why we are providing more than $2.8 billion to help Afghans develop their economy and deliver services that people depend upon.");
        list.add(list_data);

        list_data = new CommentListModal(R.drawable.profile1, "Mona", "1day ago", "And that is why we are providing more than $2.8 billion to help Afghans develop their economy and deliver services that people depend upon.");
        list.add(list_data);


        commentAdapter.notifyDataSetChanged();
    }
}
