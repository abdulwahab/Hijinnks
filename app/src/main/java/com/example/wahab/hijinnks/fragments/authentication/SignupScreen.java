package com.example.wahab.hijinnks.fragments.authentication;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.wahab.hijinnks.R;


public class SignupScreen extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_signup_screen, container, false);
        TextView tv = view.findViewById(R.id.term_condition);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(view.getContext().getResources().getString(R.string.register_line), Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv.setText(Html.fromHtml(view.getContext().getResources().getString(R.string.register_line)));
        }
        return view;

    }

}
