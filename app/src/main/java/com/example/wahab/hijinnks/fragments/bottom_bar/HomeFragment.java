package com.example.wahab.hijinnks.fragments.bottom_bar;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.wahab.hijinnks.R;
import com.example.wahab.hijinnks.activities.CommentScreen;
import com.example.wahab.hijinnks.adapter.HomeListAdapter;
import com.example.wahab.hijinnks.models.HomeListModel;

import java.util.ArrayList;
import java.util.List;

import static com.example.wahab.hijinnks.activities.BottomTabBar.mDrawerLayout;


public class HomeFragment extends Fragment {
    private List<HomeListModel> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private HomeListAdapter homeAdapter;
    ImageView comment_icon,btn_menu;
//    private DrawerLayout mDrawerLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitView(view);
        InitListener();
        InitRecyclerView();
    }

    private void InitView(View view) {
        comment_icon = (ImageView) view.findViewById(R.id.comment_icon);
        btn_menu = (ImageView) view.findViewById(R.id.menu);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_home_fragment);
        homeAdapter = new HomeListAdapter(list);
    }

    private void InitListener() {
        prepareFragmentData();

        comment_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CommentScreen.class);
                startActivity(intent);
            }
        });

        btn_menu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });
    }

    private void InitRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(homeAdapter);
    }

    private void prepareFragmentData() {
        HomeListModel list_data = new HomeListModel(R.drawable.profile_icon,"AbuBakar Butt", "Creek Drive, Afganistan", "4.3", R.drawable.img1,"Welcome to Ziro Valley 2017","December 20, 2017", "6:00 PM","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","8","20 RSVP’d","7");
        list.add(list_data);

        list_data = new HomeListModel(R.drawable.profile_icon,"AbuBakar Butt", "TX, Colony Creek Drive", "4.3", R.drawable.img,"Welcome to Ziro Valley 2017","August 20, 2017", "6:00 PM","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","8","20 RSVP’d","7");
        list.add(list_data);

        homeAdapter.notifyDataSetChanged();
    }

}
