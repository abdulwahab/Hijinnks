package com.example.wahab.hijinnks.activities;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.wahab.hijinnks.R;
import com.example.wahab.hijinnks.adapter.MenuListAdapter;
import com.example.wahab.hijinnks.fragments.bottom_bar.AddEvent;
import com.example.wahab.hijinnks.fragments.bottom_bar.HomeFragment;
import com.example.wahab.hijinnks.fragments.bottom_bar.MapViewFragment;
import com.example.wahab.hijinnks.fragments.bottom_bar.SearchFriend;
import com.example.wahab.hijinnks.fragments.bottom_bar.Setting;
import com.example.wahab.hijinnks.fragments.side_menu.EventDetails;
import com.example.wahab.hijinnks.fragments.side_menu.Profile;
import com.example.wahab.hijinnks.models.MenuListModel;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class BottomTabBar extends AppCompatActivity implements MenuListAdapter.onClickItem, View.OnClickListener {
    ImageView home, search, addEvent, addUser, setting, menu_logout;
    CircleImageView profileImage;
    MapViewFragment abc;

    private List<MenuListModel> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MenuListAdapter mAdapter;
    public static DrawerLayout mDrawerLayout;
    private String xyz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        ChangeStatusBarColor(BottomTabBar.this,"#00000000");
        setContentView(R.layout.activity_tab);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN );
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = getWindow();
////            window.addFlags(WindowManager.LayoutParams.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//            window.setStatusBarColor(Color.TRANSPARENT);
//        }

        InitView();
        InitListener();
        InitRecyclerView();

//        FragmentTransaction fragmentTransaction;
//        fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.add(R.id.frame_layout, new HomeFragment());
//        fragmentTransaction.commitAllowingStateLoss();


//        profileImage.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("ResourceAsColor")
//            @Override
//            public void onClick(View v) {
//
//                mDrawerLayout.closeDrawer(Gravity.LEFT);
//                FragmentTransaction fragmentTransaction;
//                fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.frame_layout, new Profile());
//                fragmentTransaction.commitAllowingStateLoss();
//            }
//        });
//
//        menu_logout.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("ResourceAsColor")
//            @Override
//            public void onClick(View v) {
//
//                mDrawerLayout.closeDrawer(Gravity.LEFT);
//                FragmentTransaction fragmentTransaction;
//                fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.frame_layout, new EventDetails());
//                fragmentTransaction.commitAllowingStateLoss();
//            }
//        });


//        home.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("ResourceAsColor")
//            @Override
//            public void onClick(View v) {
//
//                home.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.home_icon));
//                search.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.search));
//                addUser.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.men_add));
//                setting.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.setting));
//
//                FragmentChange(new HomeFragment());
//
//            }
//        });
//
//        search.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("ResourceAsColor")
//            @Override
//            public void onClick(View v) {
//
//                home.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.home_icon1));
//                search.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.search1));
//                addUser.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.men_add));
//                setting.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.setting));
//
//                FragmentChange(abc);
//            }
//        });
//
//        addUser.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("ResourceAsColor")
//            @Override
//            public void onClick(View v) {
//
//                home.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.home_icon1));
//                search.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.search));
//                addUser.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.men_add1));
//                setting.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.setting));
//
//                FragmentChange(new SearchFriend());
//            }
//        });
//
//
//        setting.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("ResourceAsColor")
//            @Override
//            public void onClick(View v) {
//
//                home.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.home_icon1));
//                search.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.search));
//                addUser.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.men_add));
//                setting.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.setting1));
//
//                FragmentChange(new Setting());
//
//            }
//        });
//
//
//        addEvent.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("ResourceAsColor")
//            @Override
//            public void onClick(View v) {
//
//                home.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.home_icon1));
//                search.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.search));
//                addUser.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.men_add));
//                setting.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.setting));
//
//                FragmentChange(new AddEvent());
//            }
//        });

    }

    private void InitListener() {
        TabHome();
        ChangeFragment(new HomeFragment());
        prepareMovieData();

        //Bottom tab bar button
        home.setOnClickListener(this);
        search.setOnClickListener(this);
        addUser.setOnClickListener(this);
        setting.setOnClickListener(this);
        addEvent.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        menu_logout.setOnClickListener(this);
    }

    public void ChangeFragment(Fragment fragmentName) {
        FragmentTransaction fragmentTransaction;
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (fragmentName instanceof Profile) {
            fragmentTransaction.add(R.id.frame_layout, fragmentName);
        } else {
            fragmentTransaction.replace(R.id.frame_layout, fragmentName);
        }

        fragmentTransaction.commitAllowingStateLoss();

    }

    private void InitView() {
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        abc = new MapViewFragment();
        mAdapter = new MenuListAdapter(movieList);
        profileImage = findViewById(R.id.profileImage);
        menu_logout = findViewById(R.id.menu_logout);

        recyclerView = findViewById(R.id.menu_recycler_view);
        mDrawerLayout = findViewById(R.id.drawer_new);

        //Bottom Tab bar button id's
        addUser = findViewById(R.id.add_user);
        search = findViewById(R.id.search);
        setting = findViewById(R.id.setting);
        addEvent = findViewById(R.id.add_event);
        home = findViewById(R.id.home);
    }

    private void InitRecyclerView() {

        mAdapter.setClick(BottomTabBar.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home:
                TabHome();
                ChangeFragment(new HomeFragment());
                break;
            case R.id.search:
                TabSearch();
                ChangeFragment(abc);
                break;
            case R.id.add_user:
                TabAdd();
                ChangeFragment(new SearchFriend());
                break;
            case R.id.setting:
                TabSetting();
                ChangeFragment(new Setting());
                break;
            case R.id.add_event:
                TabEvent();
                ChangeFragment(new AddEvent());
                break;
            case R.id.profileImage:
                TabProfile();
                ChangeFragment(new Profile());
                break;
            case R.id.menu_logout:
                TabEventD();
                ChangeFragment(new EventDetails());
                break;
        }
    }

    public void TabHome() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN );
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        home.setImageDrawable(getResources().getDrawable(R.drawable.home_icon));
        search.setImageDrawable(getResources().getDrawable(R.drawable.search));
        addUser.setImageDrawable(getResources().getDrawable(R.drawable.men_add));
        setting.setImageDrawable(getResources().getDrawable(R.drawable.setting));
    }

    public void TabSearch() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        home.setImageDrawable(getResources().getDrawable(R.drawable.home_icon1));
        search.setImageDrawable(getResources().getDrawable(R.drawable.search1));
        addUser.setImageDrawable(getResources().getDrawable(R.drawable.men_add));
        setting.setImageDrawable(getResources().getDrawable(R.drawable.setting));
    }

    public void TabAdd() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        home.setImageDrawable(getResources().getDrawable(R.drawable.home_icon1));
        search.setImageDrawable(getResources().getDrawable(R.drawable.search));
        addUser.setImageDrawable(getResources().getDrawable(R.drawable.men_add1));
        setting.setImageDrawable(getResources().getDrawable(R.drawable.setting));
    }

    public void TabSetting() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        home.setImageDrawable(getResources().getDrawable(R.drawable.home_icon1));
        search.setImageDrawable(getResources().getDrawable(R.drawable.search));
        addUser.setImageDrawable(getResources().getDrawable(R.drawable.men_add));
        setting.setImageDrawable(getResources().getDrawable(R.drawable.setting1));
    }

    public void TabEvent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        home.setImageDrawable(getResources().getDrawable(R.drawable.home_icon1));
        search.setImageDrawable(getResources().getDrawable(R.drawable.search));
        addUser.setImageDrawable(getResources().getDrawable(R.drawable.men_add));
        setting.setImageDrawable(getResources().getDrawable(R.drawable.setting));
    }

    public void TabProfile() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN );
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    public void TabEventD() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    private void prepareMovieData() {
        MenuListModel movie = new MenuListModel(R.drawable.menu_feed_icon, "Feed");
        movieList.add(movie);

        movie = new MenuListModel(R.drawable.eye_icon, "RSVP’d Events");
        movieList.add(movie);

        movie = new MenuListModel(R.drawable.menu_fav_icon, "Favourites");
        movieList.add(movie);

        movie = new MenuListModel(R.drawable.msg_icon, "Messages");
        movieList.add(movie);

        movie = new MenuListModel(R.drawable.menu_follower_icon, "Followers");
        movieList.add(movie);

        movie = new MenuListModel(R.drawable.menu_following_icon, "Following");
        movieList.add(movie);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void itemClick(int position) {
        Log.d("itemClick: ", position + "" + movieList.get(position).getName());
        Toast.makeText(this, "" + movieList.get(position).getName(), Toast.LENGTH_SHORT).show();

        xyz = movieList.get(position).getName();

//        if ( xyz == "SearchFriend" ) {
//            Intent cinemaIntent = new Intent(this, .class);
//            startActivity(cinemaIntent);
//        }
//        else if (xyz == "Help") {
//            Intent cinemaIntent = new Intent(this, .class);
//            startActivity(cinemaIntent);
//        }
//
//        else if (xyz == "My Cart") {
//            Intent cinemaIntent = new Intent(this, .class);
//            startActivity(cinemaIntent);
//        }
//
//        else if (xyz == "My Artis") {
//            Intent cinemaIntent = new Intent(this, .class);
//            startActivity(cinemaIntent);
//        }
//
//        else if (xyz == "Settings") {
//            Intent cinemaIntent = new Intent(this, .class);
//            startActivity(cinemaIntent);
//        }
//        else if (xyz == "Upload") {
//            Intent cinemaIntent = new Intent(this, .class);
//            startActivity(cinemaIntent);
//        }


        DrawerLayout drawer = findViewById(R.id.drawer_new);
        drawer.closeDrawer(Gravity.LEFT);

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public static void ChangeStatusBarColor(Activity activity, String color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void FragmentChange(Fragment fragment_name) {
        FragmentTransaction fragmentTransaction;
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment_name);
        fragmentTransaction.commitAllowingStateLoss();
    }
}
