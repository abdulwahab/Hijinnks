package com.example.wahab.hijinnks.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wahab.hijinnks.R;
import com.example.wahab.hijinnks.models.CommentListModal;

import java.util.List;

/**
 * Created by Wahab on 1/9/2018.
 */

public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.MyViewHolder> {
    private List<CommentListModal> List_view;


    public CommentListAdapter(List<CommentListModal> list_view) {
        List_view = list_view;
    }

    @Override
    public CommentListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_list, parent, false);

        return new CommentListAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(CommentListAdapter.MyViewHolder holder, int position) {
        CommentListModal frag = List_view.get(position);

        holder.user_profile.setImageResource(frag.getUser_profile());
        holder.user_name.setText(frag.getUser_name());
        holder.comment_date.setText(frag.getComment_date());
        holder.comment.setText(frag.getComment());

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView  user_profile;
        public TextView user_name;
        public TextView comment_date;
        public TextView comment;

        public MyViewHolder(View view) {
            super(view);

            user_profile = (ImageView) view.findViewById(R.id.profile_img_comment);
            user_name = (TextView) view.findViewById(R.id.comment_userName);
            comment_date = (TextView) view.findViewById(R.id.comment_date);
            comment = (TextView) view.findViewById(R.id.comment);

        }
    }

    @Override
    public int getItemCount() {
        return List_view.size();
    }
}
