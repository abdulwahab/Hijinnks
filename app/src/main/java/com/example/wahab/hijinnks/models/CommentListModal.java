package com.example.wahab.hijinnks.models;

import android.widget.ScrollView;

/**
 * Created by Wahab on 1/9/2018.
 */

public class CommentListModal {
    private int  user_profile;
    private String user_name;
    private String comment_date;
    private String comment;

    public CommentListModal(int user_profile, String user_name, String comment_date, String comment) {
        this.user_profile = user_profile;
        this.user_name = user_name;
        this.comment_date = comment_date;
        this.comment = comment;
    }

    public void setUser_profile(int user_profile) {
        this.user_profile = user_profile;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setComment_date(String comment_date) {
        this.comment_date = comment_date;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getUser_profile() {
        return user_profile;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getComment_date() {
        return comment_date;
    }

    public String getComment() {
        return comment;
    }
}
