package com.example.wahab.hijinnks.models;

/**
 * Created by Wahab on 1/11/2018.
 */

public class VideoDetailmodel {

    private String heading;
    private String video_date;
    private String video_time;
    private String like_count;
    private String rvs_count;

    public VideoDetailmodel(String heading, String video_date, String video_time, String like_count, String rvs_count) {
        this.heading = heading;
        this.video_date = video_date;
        this.video_time = video_time;
        this.like_count = like_count;
        this.rvs_count = rvs_count;
    }

    public String getHeading() {
        return heading;
    }

    public String getVideo_date() {
        return video_date;
    }

    public String getVideo_time() {
        return video_time;
    }

    public String getLike_count() {
        return like_count;
    }

    public String getRvs_count() {
        return rvs_count;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public void setVideo_date(String video_date) {
        this.video_date = video_date;
    }

    public void setVideo_time(String video_time) {
        this.video_time = video_time;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public void setRvs_count(String rvs_count) {
        this.rvs_count = rvs_count;
    }
}
